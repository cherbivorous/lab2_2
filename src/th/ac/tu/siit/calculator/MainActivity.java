package th.ac.tu.siit.calculator;



import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ((Button)findViewById(R.id.num0)).setOnClickListener(this);
        ((Button)findViewById(R.id.num1)).setOnClickListener(this);
        ((Button)findViewById(R.id.num2)).setOnClickListener(this);
        ((Button)findViewById(R.id.num3)).setOnClickListener(this);
        ((Button)findViewById(R.id.num4)).setOnClickListener(this);
        ((Button)findViewById(R.id.num5)).setOnClickListener(this);
        ((Button)findViewById(R.id.num6)).setOnClickListener(this);
        ((Button)findViewById(R.id.num7)).setOnClickListener(this);
        ((Button)findViewById(R.id.num8)).setOnClickListener(this);
        ((Button)findViewById(R.id.num9)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    double input1 = 0.0;
    String sign = "";
	String output ="0";
    
	@Override
	public void onClick(View v) {
		//10double input2 = 0.0;
		//int count = 0;
		
		int id = v.getId();
		TextView tvOutput = (TextView)findViewById(R.id.output);
		// = tvOutput.getText().toString();
		
		switch(id) {
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			if(output.equals("0")) {
				output = "";
			}
			output = output + ((Button)v).getText().toString();
			tvOutput.setText(output);
			break;
		}
		TextView operator = (TextView)findViewById(R.id.operator);
		
		if (id == R.id.ac) {
			  input1 = 0.0;
			     sign = "";
				 output ="0";
			tvOutput.setText(String.valueOf(input1));
			operator.setText(String.valueOf(""));
		}
		else if (id == R.id.bs) {
			int length = output.length();
			if (length > 1) {
				output = output.substring(0, output.length()-1);
			} else {
				output = "0";
			}
			tvOutput.setText(output);
		}
		else if (id == R.id.dot) {
			output += ".";
			tvOutput.setText(output);
		}
		else if (id == R.id.equ) {
			double input2 = Double.parseDouble(output);
			if (sign.equals("+")){
				input1 = input1 + input2;
			}
			else if (sign.equals("-")){
				input1 = input1 - input2;
			}
			else if (sign.equals("*")){
				input1 = input1 * input2;
			}
			else if (sign.equals("/")){
				input1 = input1 / input2;
			}
			output = String.valueOf(input1);
			tvOutput.setText(String.valueOf(input1));	
		}
		
		else if (id == R.id.add) {
			if (sign.equals("")) {
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("+"));
				sign = "+";
				output = "0";
			}
			else {
				
				if (sign.equals("+"))
				{
					double input2 = Double.parseDouble(output);
					input1 = input1 + input2;
					output = String.valueOf(input1);
					tvOutput.setText(String.valueOf(input1));
					
				}
				
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("+"));
				sign = "+";
				output = "0";
				
			}
			
		} 
		else if (id == R.id.sub) {
			if (sign.equals("")) {
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("-"));
				sign = "-";
				output = "0";
			}
			else {
				
				if (sign.equals("-"))
				{
					double input2 = Double.parseDouble(output);
					input1 = input1 - input2;
					output = String.valueOf(input1);
					tvOutput.setText(String.valueOf(input1));
					
				}
				
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("-"));
				sign = "-";
				output = "0";
				
			}
		} 
		else if (id == R.id.mul) {
			if (sign.equals("")) {
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("x"));
				sign = "*";
				output = "0";
			}
			else {
				
				if (sign.equals("*"))
				{
					double input2 = Double.parseDouble(output);
					input1 = input1 * input2;
					output = String.valueOf(input1);
					tvOutput.setText(String.valueOf(input1));
					
				}
				
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("x"));
				sign = "*";
				output = "0";
				
			}
		} 
		else if (id == R.id.div) {
			if (sign.equals("")) {
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("/"));
				sign = "/";
				output = "0";
			}
			else {
				
				if (sign.equals("/"))
				{
					double input2 = Double.parseDouble(output);
					input1 = input1 / input2;
					output = String.valueOf(input1);
					tvOutput.setText(String.valueOf(input1));
					
				}
				
				input1 = Double.parseDouble(output);
				operator.setText(String.valueOf("/"));
				sign = "/";
				output = "0";
				
			}
		} 
		
		

	}
    
}
